# BOXES v0.3

In our [previous lesson](lesson2.run.html) we ended with something like a line
of army ants, all our boxes lined up. Let's make it better by making them
organize themselves in rows.

The code for the `Box` class is just like before. Since you have seen it twice
already, I will not show it. You can still see it in our <a
href="lesson3.py" target="_blank">full listing</a>



But now, let's organize our boxes in rank and file. In fact, let's put our
many boxes inside a big box.

<div class='source_title'><a href="code/lesson3.py.html" target="_blank">lesson3.py</a></div>

```python
&&&16
big_box = Box(0, 0, 30, 50)

```

We will get our boxes one at a time, put the first in 0,0 and the next one right
at its right, and so on, and when we are about to step outside of the big box,
we go back to the left, a little down, and do it all over again.

<div class='source_title'><a href="code/lesson3.py.html" target="_blank">lesson3.py</a></div>

```python
&&&18
# We add a "separation" constant so you can see the boxes individually
separation = .2


def layout(_boxes):
    # Because we modify the box list, we will work on a copy
    boxes = _boxes[:]
    # The 1st box is at 0,0 so no need to do anything with it, right?
    previous = boxes.pop(0)
    while boxes:
        # We take the new 1st box
        box = boxes.pop(0)
        # And put it next to the other
        box.x = previous.x + previous.w + separation
        # At the same vertical location
        box.y = previous.y
        # But if it's too far to the right...
        if (box.x + box.w) > big_box.w:
            # We go all the way left and a little down
            box.x = 0
            box.y = previous.y + previous.h + separation
        previous = box


layout(many_boxes)

```

And now we can draw it. Just so we are sure we are staying inside the
big box, we will draw it too, in light blue.

<div class='source_title'><a href="code/lesson3.py.html" target="_blank">lesson3.py</a></div>

```python
&&&44
import svgwrite


def draw_boxes(boxes, fname, size):
    dwg = svgwrite.Drawing(fname, profile="full", size=size)
    # Draw the "big box"
    dwg.add(
        dwg.rect(
            insert=(f"{big_box.x}cm", f"{big_box.y}cm"),
            size=(f"{big_box.w}cm", f"{big_box.h}cm"),
            fill="lightblue",
        )
    )
    # Draw all the boxes
    for box in boxes:
        dwg.add(
            dwg.rect(
                insert=(f"{box.x}cm", f"{box.y}cm"),
                size=(f"{box.w}cm", f"{box.h}cm"),
                fill="red",
            )
        )
    dwg.save()


# Make the visible part of the drawing larger to show big_box
draw_boxes(many_boxes, "lesson3.svg", ("50cm", "90cm"))

```

And here is the output:

![lesson3.svg](lesson3.svg)

That is strangely satisfying! Of course we are doing something wrong in that
we are overflowing the big box vertically.

So, we could have more than one big box. And use them as pages?

----------

Further references:

* Full source code for this lesson <a href="lesson3.py" target="_blank">lesson3.py</a>
* <a href="diffs/lesson2_lesson3.html" target="_blank">Difference with code from last lesson</a>

