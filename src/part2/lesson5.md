# Boxes v0.16

In the [previous lesson](lesson4.run.html) we created a test with some basic
behavior we expected, it failed, we fixed the code, and it passed. Nice!

We specifically tested the part in `justify_row` that handles lines without
any spaces or hyphens. Let's add more tests to see if `justify_row` does the
right thing. This is just thinking behavior and creating tests for it.

Let's try some very basic behavior we want when there are spaces:

If we feed "aaaaa aaaaa" to `justify_row` we expect to have the row fully
justified, and the space in the middle to have grown a lot.

```python-include-norun:tests/test_justify_row.py:30:52
```

We can tell `pytest` to run only this specific test:

```bash
$ env PYTHONPATH=. pytest tests/test_justify_row.py::test_justify_with_spaces

============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: code/lesson5, inifile:
collected 1 item

tests/test_justify_row.py .                                               [100%]

=========================== 1 passed in 0.24 seconds ===========================
```

Nice!

How about other behaviors? Our function removes spaces from the right:

```python-include-norun:boxes.py:66:68
```

Let's do a test to make sure we do that!

```python-include-norun:tests/test_justify_row.py:76:94
```

That works too!

How about lines with a newline character at the end? If we have "aaa aaa
aaa\n" then that should not actually be justified, right?

```python-include-norun:tests/test_justify_row.py:56:73
```

```bash
$ env PYTHONPATH=. pytest tests/test_justify_row.py::test_justify_ends_with_newline

[skipping]

        # The last element should NOT be flushed-right
>       assert row[-1].x + row[-1].w == 10
E       assert (49.0 + 1) == 10
E        +  where 49.0 = Box(49.0, 0, 1, 0, "\n").x
E        +  and   1 = Box(49.0, 0, 1, 0, "\n").w

tests/test_justify_row.py:68: AssertionError
```

And it fails. That is not surprising because if you look at our implementation
of `justify_row()` it doesn't check for newlines at all! That is because we
are handling that case in `layout()` instead. This is arguably correct. On the
other hand, if we want to encapsulate the justification of a row in this
function it makes sense to move that here.

At this point, however, that is not implemented but it's not really a bug
because it *is* handled. So, we can mark this test as an "expected failure"
and put a pin in it so we can come back to it. We mark it as expected failure
using the `pytest.mark.xfail` decorator, and add a `FIXME` comment to remember
to come back.

```python-include-norun:tests/test_justify_row.py:55:57
```

The other thing this function does is put everything in an actual row
considering "separation":

```python-include-norun:boxes.py:82:84
```

Let's test if it does indeed do that!

```python-include-norun:tests/test_justify_row.py:97:124
```

And we finish running our whole test suite and see what happens.

```bash
$ env PYTHONPATH=. pytest
============================= test session starts ==============================
platform linux -- Python 3.6.4, pytest-3.5.0, py-1.5.3, pluggy-0.6.0
rootdir: part2/code/lesson5, inifile:
collected 5 items

tests/test_justify_row.py ..x..                                           [100%]

===================== 4 passed, 1 xfailed in 0.37 seconds ======================
```

Notice how we have 4 passing tests and an `xfail`.

What we have been working on is called "test coverage". We now have tests that
cover the expected behavior of `justify_row`. That means that when, in the
next lesson, we **change** that behavior, we can be fairly sure we **know**
how we are changing it.

------

* Full source code for this lesson <a href="boxes.py.html" target="_blank">boxes.py</a>
* <a href="code/diffs/lesson5_diff.html" target="_blank">Difference with code from last lesson</a>
