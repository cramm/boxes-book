# GitLab & GitHub

In the previous chapter we covered *local* usage of Git. We learned how to
keep our code versioned using it, how to go back in time, how to undo changes,
how to keep separate *branches* of our code to work in more than one thing at
a time, and how to move changes from one branch to another. That is a lot!

In this chapter we will continue doing more or less the same thing, but with a
twist: how about having branches that don't live in your own computer?

If you worked in a project with someone else, you could each work in your own
copy of the repo, and then sync changes with each other!

Or, if there was a company that provided the service, there could be a
"central" copy of your repository, and you could both sync your codes with it,
and then no matter how many people worked on the project, **all of them**
could cooperate!

Good news! There are a number of companies that provide that service. For
free! I am going to focus on [Gitlab](gitlab.com) mostly because they provide
not only a service to give you public repositories of your projects for free,
but also **private** ones for things you don't want to share.

When I am giving specific instructions, I will give alternatives for gitlab
and github. They are very, very similar.

## Preparation

Create an account for yourself. Mine is `ralsina` so I will use that in the
examples, feel free to use your own!

The details on how to setup the account I leave to you, they are no different to any other online service. Except for SSH keys.

All the communication between you and the site is private and encrypted. To do
that, you need to give the site your "public key". This also allows the site
to *know* you are who you say you are. You can *sign* your code using your keys.

So, if you have a `~/.ssh/id_rsa.pub` file, good. If you don't you can create one
with this command:

```sh
ssh-keygen -t rsa -C "your_email@example.com"
```

And then that will create the `~/.ssh/id_rsa.pub` file. That file is your **public key.** There is also one without the `.pub` extension. That is your **private key**. **NEVER SHARE THAT ONE WITH ANYONE. EVER EVER.**

**REALLY NEVER EVER SHARE THAT ONE.**

Open that public file in a text editor and copy its contents.

<dl>
<dt>In gitlab:</dt>
    <dd>In the top-right menu ▶ Settings ▶ SSH keys ▶ Paste the public key.</dd>
<dt>In github:</dt>
    <dd>In the top-right menu ▶ Settings ▶ SSH and GPG keys ▶ New SSH key ▶ Paste the public key.</dd>
</dl>

