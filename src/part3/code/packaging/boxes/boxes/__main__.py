"""
Usage:
    boxes <input> <output> [--page-size=<WxH>] [--separation=<sep>]
    boxes --version
"""

from docopt import docopt
import boxes


def main():
    """The main routine of Boxes."""
    arguments = docopt(__doc__, version=f"Boxes {boxes.__version__}")

    if arguments["--page-size"]:
        p_size = [int(x) for x in arguments["--page-size"].split("x")]
    else:
        p_size = (30, 50)

    if arguments["--separation"]:
        separation = float(arguments["--separation"])
    else:
        separation = 0.05

    boxes.convert(
        input=arguments["<input>"],
        output=arguments["<output>"],
        page_size=p_size,
        separation=separation,
    )


if __name__ == "__main__":
    main()
