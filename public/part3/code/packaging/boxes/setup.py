#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import io
from setuptools import setup, find_packages


setup(
    name="boxes",
    version="0.14",
    description="A SVG text layout engine",
    keywords="boxes,book,svg,typesetting",
    author="Roberto Alsina",
    author_email="ralsina@netmanagers.com.ar",
    url="https://ralsina.gitlab.io/boxes-book/",
    license="MIT",
    long_description=io.open(
        "./README.rst", "r", encoding="utf-8"
    ).read(),
    platforms="any",
    zip_safe=False,
    python_requires=">=3.6",
    # http://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python",
        "Topic :: Text Processing",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
    ],
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    install_requires=["svgwrite", "pyphen", "docopt"],
    entry_points={"console_scripts": ["boxes = boxes.__main__:main"]},
)
