# Boxes-Book

This is the source code for my book available at https://ralsina.gitlab.io/boxes-book/

The build process is pretty manual because I did not expect anyone else to use it but here it is:

* Get `gitbook` (pretty recent version)
* Run `gitbook install` (to get all gitbook plugins)
* Get graphviz 2.40.1 (yes, that is newer than the one you have)
* Get chroma from https://github.com/alecthomas/chroma yes, I know it's an esoteric dependency and you have no easy way to install it.
* Setup a python >= 3.6 virtualenv
* pip install -r requirements.txt
* make / make serve / make pdf


